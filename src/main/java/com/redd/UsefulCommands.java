package com.redd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.table.*;

import java.io.PrintWriter;

@ShellComponent
@ShellCommandGroup
public class UsefulCommands
{
    private final String[] algorithmsNames = {"RSA", "Elliptic Curves", "Diffie Hellman", "DSA"};
    private final Integer[] keysSizesPart1 = {1024, 112, 512, 512};
    private final Integer[] keysSizesPart2 = {2048, 256, 1024, 1024};
    private final Integer[] keysSizesPart3 = {4096, 571, 1536, 2048};
    private final Integer[] keysSizesPart4 = {8192, null, 1536, 3072};
    private final Integer[] keysSizesPart5 = {12228, null, 3072, null};
    private final Integer[] keysSizesPart6 = {16384, null, 4096, null};
    private final Integer[] keysSizesPart7 = {null, null, 6144, null};
    private final Integer[] keysSizesPart8 = {null, null, 8192, null};

    @Autowired
    private PrintWriter writer;

    @ShellMethod(value = "Table of supported algorithms and size restrictions", key = "table")
    public Table tableOfKeysSizes()
    {

        writer.println();
        writer.println("Summary of cryptographic algorithms - according to NIST");
        writer.println(
                "https://www.cryptomathic.com/news-events/blog/summary-of-cryptographic-algorithms-according-to-nist");
        writer.println();

        Object[][] data = new Object[][]{
                algorithmsNames, keysSizesPart1
                , keysSizesPart2, keysSizesPart3
                , keysSizesPart4, keysSizesPart5
                , keysSizesPart6, keysSizesPart7
                , keysSizesPart8
        };

        TableModel model = new ArrayTableModel(data).transpose();
        Table table = new TableBuilder(model)
                .addFullBorder(BorderStyle.oldschool)
                .build();

        return table;
    }
}