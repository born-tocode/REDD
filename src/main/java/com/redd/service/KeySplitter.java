package com.redd.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class KeySplitter
{
    private final ServiceFacade serviceFacade;

    public KeySplitter(ServiceFacade serviceFacade)
    {
        this.serviceFacade = serviceFacade;
    }

    @SneakyThrows
    public void run(List<byte[]> encodedKeys, boolean fileSwitch)
    {
        List<ByteArrayOutputStream> splittedKeysBuffer = splitKeys(encodedKeys);

        if (fileSwitch)
        {
            serviceFacade
                    .fileKeysBuilder()
                    .run(splittedKeysBuffer);
        }
        else
        {
            serviceFacade
                    .viewKeysBuilder()
                    .run(splittedKeysBuffer);
        }
    }

    private List<ByteArrayOutputStream> splitKeys(List<byte[]> encodedKeys)
    {
        List<ByteArrayOutputStream> splittedKeysBuffer = new ArrayList<>();
        splittedKeysBuffer.add(0, new ByteArrayOutputStream());
        splittedKeysBuffer.add(1, new ByteArrayOutputStream());
        int nextStream = 0;

        for (byte[] key : encodedKeys)
        {
            final int length = 54;
            final int fullLengthOfSegment = length + 1;
            final int newLine = 55;
            final int newKeyLength = key.length + key.length / fullLengthOfSegment;
            final byte byteOfNewLine = 10;
            int position = 0;
            int count = 0;
            byte[] newKey = new byte[newKeyLength];

            for (int i = 0; i < key.length; i++)
            {
                if (count==newLine)
                {
                    count = 0;
                    newKey[i] = byteOfNewLine;
                }
                else
                {
                    count++;
                    newKey[i] = key[position];
                    position++;
                }
            }

            try
            {
                splittedKeysBuffer
                        .get(nextStream)
                        .write(newKey);
                nextStream++;
            }
            catch (IOException e)
            {
                log.error("I can't write key to ByteArrayOutputStream", e);
            }
        }

        return splittedKeysBuffer;
    }
}
