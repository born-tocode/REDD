package com.redd.service;

import org.springframework.stereotype.Service;

@Service
public class ServiceFacade
{

    public KeysGenerator keysGenerator() throws IllegalAccessException, InstantiationException
    {
        return KeysGenerator.class.newInstance();
    }

    public FileKeysBuilder fileKeysBuilder() throws IllegalAccessException, InstantiationException
    {
        return FileKeysBuilder.class.newInstance();
    }

    public KeySplitter keySplitter() throws IllegalAccessException, InstantiationException
    {
        return KeySplitter.class.newInstance();
    }

    public ViewKeysBuilder viewKeysBuilder() throws IllegalAccessException, InstantiationException
    {
        return ViewKeysBuilder.class.newInstance();
    }
}
