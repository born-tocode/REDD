package com.redd.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Slf4j
@Service
public class ViewKeysBuilder
{
    
    @Autowired
    private PrintWriter printWriter;
    
    public void run(List<ByteArrayOutputStream> splittedKeysBuffer)
    {
        
        splittedKeysBuffer.forEach(key ->
                                   {
                                       try
                                       {
                                           key.writeTo(System.out);
                                           printWriter.println();
                                           printWriter.println();
                                       }
                                       catch (IOException e)
                                       {
                                           log.error("I can't write keys to buffer.", e);
                                       }
                                   });
    }
}
