package com.redd;

import com.redd.service.ServiceFacade;
import lombok.SneakyThrows;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
@ShellCommandGroup
public class MainCommands
{

    private final ServiceFacade serviceFacade;

    public MainCommands(ServiceFacade serviceFacade)
    {
        this.serviceFacade = serviceFacade;
    }

    @SneakyThrows
    @ShellMethod(value = "Generate new key pair")
    public void generate(
            @ShellOption(value = {"-a", "--algorithm"}) String algorithm,
            @ShellOption(value = {"-k", "--keysize"}) int keySize,
            @ShellOption(value = {"-f", "--file",}, defaultValue = "false") boolean fileSwitch)
    {
        serviceFacade
                .keysGenerator()
                .run(algorithm, keySize, fileSwitch);

    }

}
